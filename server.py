from bottle import run
from lib.routes import items, oauth, ping

run(host='0.0.0.0', port=8081, reloader=True)
