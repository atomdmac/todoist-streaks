import json
from datetime import datetime
import db

def item_is_recurring(item):
    if item['due'] == None:
        return False
    return item['due']['is_recurring']


def get_recurring_items_from_response(todoist_response):
    items = todoist_response['items']
    items = [itm for itm in items if item_is_recurring(itm)]
    return items

def save_item(cur, item):
    cur.execute("""
        INSERT INTO items (
            remote_id, content, streak_count, next_due_date, raw_data)
        VALUES (%s, %s, %s, %s, %s)
        ON CONFLICT (remote_id)
        DO
            UPDATE SET (
                remote_id, content, streak_count, next_due_date, raw_data)
                = (
                    EXCLUDED.remote_id,
                    EXCLUDED.content,
                    EXCLUDED.streak_count,
                    EXCLUDED.next_due_date,
                    EXCLUDED.raw_data);
        """,
        (item['id'], item['content'], 0, item['due']['date'], '{}'))

def save_items(items):
    print('saving items...')
    cur = db.conn.cursor()
    for itm in items:
        save_item(cur, itm)
    db.conn.commit()
    cur.close()
    print('done saving items.')

def load_from_disk(path):
    print('Loading data...')
    f = open(path, 'r')
    return json.load(f)

# Sample usage:
# save_items(
#     get_recurring_items_from_response(
#         load_from_disk('./sample.json')))

