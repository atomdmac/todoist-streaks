import sys

"""
Return the last error that was generated.
"""
def getLastError():
    return sys.exc_info()[0]
