import psycopg2
from .env import POSTGRES_HOST, POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD

conn = psycopg2.connect(
        "host=%s dbname=%s user=%s password=%s" % (
            POSTGRES_HOST,
            POSTGRES_DB,
            POSTGRES_USER,
            POSTGRES_PASSWORD
            )
        )

