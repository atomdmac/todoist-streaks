import json
from .error import getLastError

"""
Format the given credentials as JSON and save them to disk.
"""
def saveCredentials(tokenType, accessToken):
    data = {}
    data['tokenType'] = tokenType
    data['accessToken'] = accessToken
    with open('credentials.json', 'w') as outfile:
        json.dump(data, outfile)

"""
Load JSON-formated credentials from disk and place them into a `credentials`
parameter for later use.
"""
def loadCredentials():
    with open('credentials.json', 'r') as json_file:
        data = json.load(json_file)
        credentials = {
            'tokenType': data['tokenType'],
            'accessToken': data['accessToken']
            }

    print('Credentials have been loaded from disk...')
    return credentials

