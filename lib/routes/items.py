import json
import psycopg2
from bottle import post, request, route
from todoist.api import TodoistAPI
from ..util import fileio
from ..util import db
from ..util.env import (
        POSTGRES_HOST,
        POSTGRES_DB,
        POSTGRES_USER,
        POSTGRES_PASSWORD
        )


"""
Return all data for a user with the given access token.
"""
@route('/sync')
def getTasks():
    credentials = fileio.loadCredentials()
    # TODO: Allow credentials to be parameterized.
    api = TodoistAPI(credentials['accessToken'])
    api.reset_state()
    r = api.sync()
    return r


# TODO: Add request body validation
@post('/item/add')
def addTask():
    remote_id = request.json['remote_id']
    content = request.json['content']
    streak_count = request.json['streak_count']
    raw_data = json.dumps(request.json['raw_data'])

    cur = db.conn.cursor()
    cur.execute("""
        INSERT INTO items (remote_id, content, streak_count, raw_data)
        VALUES (%s, %s, %s, %s);
        """,
        (remote_id, content, streak_count, raw_data))

    db.conn.commit()
    cur.close()

    return 'Data saved 👍'


@post('/webhook')
def webhook():
    print(request.json)
