from bottle import redirect, request, route
import requests as makeRequest
from ..util.env import CLIENT_ID, CLIENT_SECRET, SCOPE, STATE
from ..util import fileio


"""
Start the OAuth process with Todoist
"""
@route('/oauth/start')
def oauthStart():
    uri = 'https://todoist.com/oauth/authorize?client_id=%s&scope=%s&state=%s'
    redirect(uri % (CLIENT_ID, SCOPE, STATE))


"""
Receive the OAuth code from Todoist, exchange it for an access token, and then
return the access token to the user.
"""
@route('/oauth/code')
def oauthCode():
    error = request.query.error
    if error:
        return '😢 %s' % error

    payload = {}
    payload['client_id'] = CLIENT_ID
    payload['client_secret'] = CLIENT_SECRET
    payload['code'] = request.query.code

    r = makeRequest.post('https://todoist.com/oauth/access_token', data=payload)
    data = r.json()
    tokenType = data['token_type']
    accessToken = data['access_token']

    fileio.saveCredentials(tokenType, accessToken)

    return '%s / %s' % (tokenType, accessToken)
