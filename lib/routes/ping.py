from bottle import request, route


@route('/ping')
def ping():
    return 'PONG!'
