CREATE TABLE items (
  id SERIAL PRIMARY KEY,
  remote_id BIGINT NOT NULL UNIQUE,
  content varchar(255) NOT NULL,
  streak_count INT NOT NULL DEFAULT 0,
  next_due_date TIMESTAMPTZ NOT NULL,
  raw_data JSONB NOT NULL
);
